Fair Choice
===========

Is a stateless Single Page App (SPA) with isolated side-effects, that provides
true randomness when shuffling a sequence of sensitive data. Please read the
“About” section before using the SPA in order to understand privacy and design.

	http://spisemisu.com/spa/fairchoice/
