{- Fair Choice, (c) 2017 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0

   Derived from github.com/rundis/albums:
   (c) 2015 rundis, opensource.org/licenses/MIT
-}


module Router exposing (..)

import UrlParser exposing (Parser, (</>), int, oneOf, s, top)
import Navigation exposing (Location)
import Html.Attributes exposing (href, attribute)
import Html exposing (Html, Attribute, a)
import Html.Events exposing (onWithOptions)
import Json.Decode as Json


type Route
    = FairPage
    | AboutPage


routeParser : Parser (Route -> a) a
routeParser =
    UrlParser.oneOf
        [ UrlParser.map FairPage top
        , UrlParser.map AboutPage (s "about")
        ]


decode : Location -> Maybe Route
decode location =
    UrlParser.parseHash routeParser location


encode : Route -> String
encode route =
    case route of
        FairPage ->
            "#"

        AboutPage ->
            "#about"


navigate : Route -> Cmd msg
navigate route =
    -- Navigation.modifyUrl (encode route)
    Navigation.newUrl (encode route)


linkTo : Route -> List (Attribute msg) -> List (Html msg) -> Html msg
linkTo route attrs content =
    a ((linkAttrs route) ++ attrs) content


linkAttrs : Route -> List (Attribute msg)
linkAttrs route =
    let
        path =
            encode route
    in
        [ href path
        , attribute "data-navigate" path
        ]


catchNavigationClicks : (String -> msg) -> Attribute msg
catchNavigationClicks tagger =
    onWithOptions "click"
        { stopPropagation = True
        , preventDefault = True
        }
        (Json.map tagger (Json.at [ "target" ] pathDecoder))


pathDecoder : Json.Decoder String
pathDecoder =
    Json.oneOf
        [ Json.at [ "data-navigate" ] Json.string
        , Json.at [ "parentElement" ] (Json.lazy (\_ -> pathDecoder))
        , Json.fail "no path found for click"
        ]
