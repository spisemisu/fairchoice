{- Fair Choice, (c) 2017 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0

   Derived from elm-lang.org:
   (c) 2012-2015 Evan Czaplicki, opensource.org/licenses/BSD-3-Clause
-}


module About exposing (Model, Msg, init, update, view)

-- LIBS

import Html exposing (..)


-- FILES

import Common.Center as Center


-- MODEL


type alias Model =
    ()


init : Model
init =
    ()



-- MESSAGE


type Msg
    = Show



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Show ->
            ( model, Cmd.none )



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ Center.markdown "600px" about ]


about : String
about =
    """

### Release Notes

<table class="releases">
  <tbody>
    <tr>
      <td>0.11.0</td>
      <td>Initial λ Release</td>
      <td>2017 Jul</td>
    </tr>
  </tbody>
</table>

<br>

### Features, Data Protection and Design

* **Local SPA** with **isolated side-effects**: Once the SPA is retrieved by the
  browser, there will be no further connections to the hosting server. There
  will *only* be sent requests to [RANDOM.ORG][randomorg], see **True
  randomness**, which are the *only* allowed side-effects the application is
  designed to perform besides rendering the SPA HTML output of course.

* **True randomness**: Most random numbers used in computer programs are
    pseudo-random, which means they are generated in a predictable fashion using
    a mathematical formula. [RANDOM.ORG][randomorg] offers true random numbers
    to anyone on the Internet. The randomness comes from atmospheric noise,
    which for many purposes is better than the pseudo-random number algorithms
    typically used in computer programs: “Let the (weather) Gods decide”.

> **Note**: Sensitive data is not sent to [RANDOM.ORG][randomorg] as that would
    be a breach in the data protection of this SPA. The only value that is sent
    to the site is the number of elements for which a sequence of random numbers
    will be retrieved. For more information on this kind of design, please see
    [RANDOM.ORG + sensitive data][randomorgsd]

* **Stateless**: No information is stored locally or remotely. By refreshing the
  browser or pressing the **Reset** button, will ensure that all sensitive data
  is no longer available.

* **No cookies** or **Google Analytics**: In order to comply with the lack of
  state as well as safeguarding the protection of the sensitive data, therefore
  none of these components will be used.

* **No HTTPS over SSL/TLS**: Encryption will not be necessary as there will be
  no remote request with sensitive data.

* **EU GDPR**: Given the [European General Data Protection Regulation][eugdpr],
  due date: 2018-05-25, we need to ensure that software complies with the
  upcoming law. A few articles that had an impact on the design and
  implementation when architecture this SPA are:
  * **Data protection by design and by default**: “... the controller shall,
    ..., implement appropriate TECHNICAL and organizational measures, ..., which
    are DESIGNED to implement data-protection principles, ..., in an effective
    manner and to integrate the necessary SAFEGUARDS into the processing in
    order to meet the requirements of this Regulation and protect the rights of
    data subjects” (Art 25.1).
  * **Principles relating to processing of personal data**:
    * “lawfulness, fairness and transparency” (Art 5.1.a)
    * “purpose limitation” (Art 5.1.b)
    * “data minimization” (Art 5.1.c)
    * “accuracy” (Art 5.1.d)
    * “storage limitation” (Art 5.1.e)
    * “integrity and confidentiality” (Art 5.1.f)

[randomorg]: https://www.random.org/faq/#Q3.2
[randomorgsd]: https://www.random.org/faq/#Q3.2
[eugdpr]: http://www.eugdpr.org

<br>

### Licensing

In order to ensure that the rights of the end-users are preserve as thought
initially by this SPA, we have come to the conclusion that correct and only form
of licensing that support this, is [copyleft][copyleft]. Therefore source code
and media content are released under **AGPL-3.0** and **CC BY-SA**,
respectively:

* [AGPL-3.0][agpl3]
  * [GitLab (master)][gitlab]
  * [GitHub (sync)][github]
* [CC BY-SA 4.0][ccbysa4]

[copyleft]: https://copyleft.org/
[agpl3]:    https://www.gnu.org/licenses/agpl-3.0.html
[ccbysa4]:  https://creativecommons.org/licenses/by-sa/4.0/
[gitlab]:   https://gitlab.com/spisemisu/fairchoice
[github]:   https://github.com/spisemisu/fairchoice

<br>

### Contribute

There are only three ways you can contribute:

* Donate well written code or create media content
* Donate design/code reviews with constructive criticism
* Donate BTC:
  * Address: 1GboZtWqCgmaZqoTHK6Nnuy54GBtuxXRHp

> **Note**: Donations to companies in Denmark can't be anonymous due to Money
    laundering legislation, therefore please ensure that your donation is linked
    to your information as we will not be using any time to refund due to
    resources limitations.

"""
