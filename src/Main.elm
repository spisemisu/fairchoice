{- Fair Choice, (c) 2017 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0

   Derived from github.com/rundis/albums:
   (c) 2015 rundis, opensource.org/licenses/MIT
-}


module Main exposing (..)

-- LIBS

import Html exposing (..)
import Navigation


-- FILES

import About
import Common.Skeleton as Skeleton
import Fair
import Router exposing (..)


-- MAIN


main : Program Never Model Msg
main =
    Navigation.program UrlChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { route : Router.Route
    , fair : Fair.Model
    , about : About.Model
    }


initialModel : Model
initialModel =
    { route = Router.FairPage
    , fair = Fair.init
    , about = About.init
    }


init : Navigation.Location -> ( Model, Cmd Msg )
init location =
    update (UrlChange location) initialModel



-- MESSAGE


type Msg
    = Fair Fair.Msg
    | About About.Msg
    | Navigate String
    | UrlChange Navigation.Location



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Fair subMsg ->
            let
                ( subModel, subCmd ) =
                    Fair.update subMsg model.fair
            in
                { initialModel | fair = subModel }
                    ! [ Cmd.map Fair subCmd
                      ]

        About subMsg ->
            let
                ( subModel, subCmd ) =
                    About.update subMsg model.about
            in
                { model | about = subModel }
                    ! [ Cmd.map About subCmd
                      ]

        UrlChange location ->
            urlUpdate location model

        Navigate url ->
            model
                ! [ Navigation.modifyUrl url
                  ]


urlUpdate : Navigation.Location -> Model -> ( Model, Cmd Msg )
urlUpdate location model =
    case (Router.decode location) of
        Nothing ->
            model
                ! [ Navigation.modifyUrl
                        (Router.encode model.route)
                  ]

        Just route ->
            { model | route = route } ! []



-- VIEW


view : Model -> Html Msg
view model =
    div
        [ Router.catchNavigationClicks Navigate
        ]
        [ Skeleton.header model.route
        , Skeleton.content (contentView model)
        , Skeleton.footer
        ]


contentView : Model -> Html Msg
contentView model =
    case model.route of
        FairPage ->
            Html.map Fair <| Fair.view model.fair

        AboutPage ->
            Html.map About <| About.view model.about



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        []



-- HELPERS
