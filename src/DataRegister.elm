{- Fair Choice, (c) 2017 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0 -}


module DataRegister exposing (Register, Sensitive(..), add, count, get, init)

import List


type Sensitive a
    = Sensitive a


type Register a
    = Data (List a)


init : () -> Register a
init () =
    Data []


add : a -> Register a -> Register a
add x (Data reg) =
    if List.any (\y -> x == y) reg then
        Data reg
    else
        Data (x :: reg)


count : (a -> Bool) -> Register a -> Int
count cond (Data reg) =
    let
        xs =
            List.filter cond reg
    in
        List.length xs


get : (a -> b) -> (a -> Bool) -> Register a -> Maybe b
get dto cond (Data reg) =
    case reg of
        [] ->
            Nothing

        x :: xs ->
            if cond x then
                Just (dto x)
            else
                get dto cond (Data xs)
