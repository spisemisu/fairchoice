{- Fair Choice, (c) 2017 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0

   Derived from elm-lang.org:
   (c) 2012-2015 Evan Czaplicki, opensource.org/licenses/BSD-3-Clause

   Derived from github.com/rundis/albums:
   (c) 2015 rundis, opensource.org/licenses/MIT
-}


module Fair exposing (Model, Msg, init, update, view)

-- LIBS

import Date
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events as Events exposing (..)
import Http exposing (..)
import Json.Decode as Decode
import List
import Regex
import String exposing (..)
import Task exposing (..)
import Time exposing (..)
import Tuple


-- FILES

import DataRegister as Data


-- MODEL


type alias Raw =
    String


type alias Datum_ =
    ( Int, Data.Sensitive String )


type alias Datum =
    String


type alias URID =
    String


type Log
    = Log ( String, String, String )


type alias Model =
    { raw : Raw
    , reg : Data.Register Datum_
    , urids : List URID
    , log : Log
    }


init : Model
init =
    Model
        ""
        (Data.init ())
        []
        (Log ( "", "", "" ))



-- MESSAGE


type Msg
    = GetTimeAndThen ModelMsg
    | GotTime ModelMsg Time


type ModelMsg
    = ParseData Raw
    | Process (Result Http.Error Raw)
    | Assign
    | Reset



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetTimeAndThen wrappedMsg ->
            ( model
            , Task.perform (GotTime wrappedMsg) Time.now
            )

        GotTime wrappedMsg time ->
            let
                ( newModel, cmd ) =
                    updateModel wrappedMsg time model
            in
                ( newModel, Cmd.map GetTimeAndThen cmd )


updateModel : ModelMsg -> Time -> Model -> ( Model, Cmd ModelMsg )
updateModel msg time model =
    let
        status s =
            ( toString s.code, s.message )

        error err =
            case err of
                BadUrl url ->
                    "Invalid url: " ++ url

                Timeout ->
                    "Request timed-out"

                NetworkError ->
                    "No Network Connection"

                BadStatus response ->
                    let
                        ( code, message ) =
                            status response.status
                    in
                        "Statuscode: " ++ code ++ " and message: " ++ message

                BadPayload _ response ->
                    let
                        ( code, message ) =
                            status response.status
                    in
                        "Statuscode: " ++ code ++ " and message: " ++ message
    in
        case msg of
            ParseData txt ->
                let
                    par =
                        parse txt
                            |> List.indexedMap
                                (\i x -> ( i, Data.Sensitive x ))

                    reg =
                        par
                            |> List.foldl
                                (\x a -> a |> Data.add x)
                                (Data.init ())
                in
                    ( { model | raw = txt, reg = reg }
                    , Cmd.none
                    )

            Assign ->
                let
                    n =
                        Data.count (\_ -> True) model.reg
                in
                    case n > 0 of
                        True ->
                            ( { model
                                | log =
                                    (log
                                        time
                                        "Assign"
                                        "Retrieving URIDs from RANDOM.ORG"
                                    )
                              }
                            , get (random n) Process
                            )

                        False ->
                            ( { model
                                | log =
                                    (log
                                        time
                                        "Assign (warning)"
                                        "Please add data"
                                    )
                              }
                            , Cmd.none
                            )

            Process result ->
                case result of
                    Ok txt ->
                        ( { model
                            | urids = parse txt |> List.map normalize
                            , log =
                                (log
                                    time
                                    "Process and Sort"
                                    "Pairing retrieved URIDs with data and sorting"
                                )
                          }
                        , Cmd.none
                        )

                    Err err ->
                        ( { model
                            | log =
                                (log
                                    time
                                    "Process and Sort (error)"
                                    (error err)
                                )
                          }
                        , Cmd.none
                        )

            Reset ->
                ( init
                , Cmd.none
                )



-- VIEW


view : Model -> Html Msg
view model =
    Html.map GetTimeAndThen (viewModel model)


viewModel : Model -> Html ModelMsg
viewModel model =
    div []
        [ splash
        , formBefore model
        , formButtons model
        , formAfter model
        ]


splash : Html ModelMsg
splash =
    let
        body =
            "Is a stateless Single Page App (SPA) with isolated side-effects, "
                ++ "that provides true randomness when shuffling a sequence of "
                ++ "sensitive data. Please read the “About” section before "
                ++ "using the SPA in order to understand data protection and design."

        description =
            div
                [ size 16 8 640 ]
                [ text body
                , br [] []
                ]
    in
        div
            [ class "splash" ]
            [ div [ size 120 16 640 ] [ text "Fair Choice" ]
            , description
            ]


size : Int -> Int -> Int -> Attribute msg
size height padding width =
    style
        [ "font-size" => (toString height ++ "px")
        , "padding" => (toString padding ++ "px 0")
        , "width" => "100%"
        , "max-width" => (toString width ++ "px")
        , "margin" => "0px auto"
        ]



-- FORM


formBefore : Model -> Html ModelMsg
formBefore model =
    section [ class "home-section" ]
        [ h1 [] [ text "Sensitive data" ]
        , formSensitiveData model
        , h1 [] [ text "Unique Random Identifiers" ]
        , formAssignedURID model
        ]


formButtons : Model -> Html ModelMsg
formButtons model =
    let
        ( action, description, date ) =
            unlog model.log

        info =
            case ( action, description, date ) == ( "", "", "" ) of
                True ->
                    div [] []

                False ->
                    div []
                        [ b [] [ text action ]
                        , text <| ": "
                        , br [] []
                        , text <| description
                        , text <| " "
                        , text <| date
                        ]
    in
        div
            [ class "splash"
            , style [ ( "margin", "100px 0" ) ]
            ]
            [ div [ class "get-started" ]
                [ label
                    [ onClick <| Assign
                    ]
                    [ text "Assign and Sort" ]
                , label
                    [ onClick <| Reset ]
                    [ text "Reset" ]
                , br [] []
                , info
                ]
            ]


formAfter : Model -> Html ModelMsg
formAfter model =
    section [ class "home-section" ]
        [ h1 [] [ text "Sorted data" ]
        , formSortedData model
        ]


formSensitiveData : Model -> Html ModelMsg
formSensitiveData model =
    div
        [ class "page-form"
        , style
            [ ( "width", "50%" )
            , ( "margin", "0 auto" )
            , ( "text-align", "center" )
            ]
        ]
        [ text "Add sequence here "
        , i [] [ text "(semicolon and/or new line separated)" ]
        , textarea
            [ style
                [ ( "display", "block" )
                , ( "margin-left", "auto" )
                , ( "margin-right", "auto" )
                ]
            , cols 64
            , rows 10
            , placeholder "foo@bar.org; baz@qux.com"
            , value <| model.raw
            , onChange <| ParseData
            ]
            []
        ]


formAssignedURID : Model -> Html msg
formAssignedURID model =
    div
        [ class "page-form"
        , style
            [ ( "width", "50%" )
            , ( "margin", "0 auto" )
            , ( "text-align", "center" )
            ]
        ]
        [ text "URID assigned to each item "
        , i [] [ text "(provided by RANDOM.ORG)" ]
        , textarea
            [ style
                [ ( "display", "block" )
                , ( "margin-left", "auto" )
                , ( "margin-right", "auto" )
                ]
            , cols 64
            , rows 10
            , readonly True
            , value <| showPaired model
            ]
            []
        ]


formSortedData : Model -> Html msg
formSortedData model =
    div
        [ class "page-form"
        , style
            [ ( "width", "50%" )
            , ( "margin", "0 auto" )
            , ( "text-align", "center" )
            ]
        ]
        [ text "Sorted ascendingly by the assigned URID "
        , i [] [ text "(-42, ..., 0, ..., 42)" ]
        , textarea
            [ style
                [ ( "display", "block" )
                , ( "margin-left", "auto" )
                , ( "margin-right", "auto" )
                ]
            , cols 64
            , rows 10
            , readonly True
            , value <| showSorted model
            ]
            []
        ]



-- HELPERS


(=>) : a -> b -> ( a, b )
(=>) =
    (,)


onChange : (String -> msg) -> Attribute msg
onChange tagger =
    on "change"
        (Decode.map tagger targetValue)


parse : String -> List String
parse raw =
    let
        remove pattern =
            Regex.replace Regex.All (Regex.regex "pattern") (\_ -> "")

        ( sc, br ) =
            ( contains ";" raw, contains "\n" raw )

        xs =
            case ( sc, br ) of
                ( True, _ ) ->
                    raw |> remove "\n" |> split ";"

                ( _, True ) ->
                    raw |> split "\n"

                ( False, False ) ->
                    [ raw ]
    in
        xs |> List.map String.trim |> List.filter (String.isEmpty >> not)


get : String -> (Result Http.Error String -> msg) -> Cmd msg
get url msg =
    Http.getString url
        |> Http.send msg


random : Int -> String
random n =
    let
        base =
            "https://www.random.org/integers/"

        query =
            "?min=-1000000000&max=1000000000&col=1&base=10&format=plain&rnd=new"

        number =
            "&num=" ++ (toString n)
    in
        base ++ query ++ number


show : Model -> List String
show model =
    let
        get id =
            Data.get
                (\( _, Data.Sensitive x ) -> x)
                (\( i, _ ) -> i == id)
                model.reg

        n =
            Data.count (\_ -> True) model.reg

        urids =
            -- List.range 0 n
            List.range 0 (n - 1)

        data =
            urids
                |> List.filterMap get
    in
        data


showPaired : Model -> String
showPaired model =
    List.map2 (,) model.urids (show model)
        |> List.foldl
            (\( x, y ) a -> a ++ x ++ ": " ++ y ++ ";\n")
            ""


showSorted : Model -> String
showSorted model =
    List.map2 (,) model.urids (show model)
        |> List.sort
        |> List.map Tuple.second
        |> List.foldl (\x a -> a ++ x ++ ";\n") ""


normalize : String -> String
normalize urid =
    let
        neg =
            String.startsWith "-" urid

        norm =
            case neg of
                True ->
                    String.dropLeft 1 urid

                False ->
                    urid

        len =
            String.length norm

        pad =
            String.repeat (10 - len) "0"

        symbol =
            if neg then
                "NEG-"
            else
                "POS-"
    in
        symbol ++ pad ++ norm


log : Time -> String -> String -> Log
log ts action description =
    let
        date =
            toString (Date.fromTime ts)
    in
        Log ( action, description, date )


unlog : Log -> ( String, String, String )
unlog (Log triple) =
    triple
