{- Fair Choice, (c) 2017 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0

   Derived from elm-lang.org:
   (c) 2012-2015 Evan Czaplicki, opensource.org/licenses/BSD-3-Clause
-}


module Common.Center exposing (leftpad, markdown, style)

-- LIBS

import Html exposing (..)
import Html.Attributes as Attr exposing (..)
import Markdown
import String


-- MARKDOWN
-- TODO: Refactor Center to something else (Markdown? really bad name !!!)


markdown : String -> String -> Html msg
markdown width string =
    div
        [ class "content"
        , style width
        ]
        [ Markdown.toHtml [] string ]



-- STYLE


style : String -> Attribute msg
style width =
    Attr.style
        [ "display" => "block"
        , "width" => width
        , "margin" => "0 auto"
        ]



-- HELPERS


leftpad : String -> Int -> Maybe Char -> String
leftpad s n c =
    let
        l =
            String.length s

        pad =
            case c of
                Just c_ ->
                    String.fromChar c_

                Nothing ->
                    "."
    in
        case l <= n of
            True ->
                (String.repeat (n - l) pad) ++ s

            False ->
                s


(=>) : a -> b -> ( a, b )
(=>) =
    (,)
