{- Fair Choice, (c) 2017 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0

   Derived from elm-lang.org:
   (c) 2012-2015 Evan Czaplicki, opensource.org/licenses/BSD-3-Clause
-}


module Common.Skeleton
    exposing
        ( bottomSplash
        , buttons
        , content
        , footer
        , header
        , splash
        )

-- LIBS

import Html exposing (..)
import Html.Attributes exposing (..)


-- FILES

import Router exposing (..)


-- CONTENT


content : Html msg -> Html msg
content body =
    div []
        [ body ]



-- HEADER


header : Route -> Html msg
header route =
    div [ id "tabs" ]
        [ Router.linkTo Router.FairPage
            [ style
                [ "position" => "absolute"
                , "left" => "0.25em"
                , "top" => "0.25em"
                ]
            ]
            [ img
                [ src "assets/logo/logo.svg"
                , style [ "width" => "50px" ]
                ]
                []
            ]
        , ul
            []
            [ tab route Router.FairPage "fair"
            , tab route Router.AboutPage "about"
            ]
        ]


tab : Route -> Route -> String -> Html msg
tab current page label =
    li []
        [ Router.linkTo page
            [ classList
                [ "tab" => True
                , "current"
                    => ((Router.encode page) == (Router.encode current)
                        -- startsWith (Router.encode page) (Router.encode current)
                        -- && (current /= Router.FairPage)
                       )
                ]
            ]
            [ text label ]
        ]



-- FOOTER


footer : Html msg
footer =
    let
        random =
            footimg
                "assets/pics/ro_logo_gh.png"
                "Powered by RANDOM.ORG"
                "https://www.random.org/"
                Nothing

        gitlab =
            footimg
                "assets/pics/gl_logo_gl.png"
                "Hosted by GitLab"
                "https://gitlab.com/"
                (Just "#000")

        agpl =
            footimg
                "assets/pics/agpl_logo_black.png"
                "Free as in Freedom, not as in Beer"
                "https://www.gnu.org/licenses/agpl.html"
                Nothing

        ccbysa =
            footimg
                "assets/pics/cc-by-sa_logo.png"
                "Media content is licensed under CC BY-SA 4.0"
                "https://creativecommons.org/licenses/by-sa/4.0/"
                Nothing
    in
        div [ class "footer" ]
            [ random
            , text "  "
            , gitlab
            , text "  "
            , agpl
            , text "  "
            , ccbysa
            , br [] []
            , text "© 2017 SPISE MISU ApS"
            ]


footimg : String -> String -> String -> Maybe String -> Html msg
footimg path text url color =
    let
        hex =
            case color of
                Just hex ->
                    hex

                Nothing ->
                    "#0f0"
    in
        a
            [ href url
            , target "_blank"
            , style
                [ ( "text-decoration", "none" )
                , ( "background-color", "#0f0" )
                ]
            ]
            [ img
                [ src path
                , title text
                , style
                    [ ( "height", "50px" )
                    , ( "background-color", hex )
                    ]
                ]
                []
            ]



-- SPLASH


splash : Html msg -> Html msg
splash buttons =
    div
        [ class "splash"
        ]
        [ div [ class "page-button" ]
            [ div [ fontsize 26 8 ]
                [--text <| "Task: " ++ (toString task.action)
                ]
            ]
        , buttons
        , div [ fontsize 16 8 ]
            [ --text "Error(s): 0% █••••••••••••• 100% (progressbar). TODO: some failure text goes here."
              text "Progress: 0% █•••••••••••••••••••••••••••••••••••••••••••••••••• 100%"
            ]
        ]


bottomSplash : Html msg -> Html msg
bottomSplash buttons =
    div
        [ class "splash"
        , style [ ( "margin", "100px 0" ) ]
        ]
        [ buttons
        , div [ fontsize 16 8 ]
            [ text "Error(s): TODO: some failure text goes here." ]
        ]


buttons : List (Html msg) -> Html msg
buttons list =
    div [ class "page-button" ]
        list



-- HELPERS


(=>) : a -> b -> ( a, b )
(=>) =
    (,)


fontsize : Int -> Int -> Attribute msg
fontsize height padding =
    style
        [ "font-size" => (toString height ++ "px")
        , "padding" => (toString padding ++ "px 0")
        ]
