#!/bin/bash

# ensure latest packages are downloaded
elm-package install --yes

# only create the .js as we will rely on a custom html file (port)
# elm-make --debug --warn --output elm.js --yes src/Main.elm
elm-make --warn --output elm.js --yes src/Main.elm

# make elm js as small as possible
./.misc/jsmin/jsmin < elm.js > elm.min.js
